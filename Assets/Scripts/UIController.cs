﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIController : MonoBehaviour
{
    [SerializeField] GameObject levelCompletepanel;
    [SerializeField] Button nextLevelButton;

    GameManager gameManager;
    AnalyticsController analytics;
    void Start()
    {
        gameManager = GameManager.GetManager();
        analytics = AnalyticsController.GetController();
        nextLevelButton.onClick.AddListener(delegate
        {
            //SceneManager.LoadScene(0);
            gameManager.GotoNextStage();
            analytics.LevelCompleted();
        });
    }

    public void LevelComplete()
    {
        levelCompletepanel.SetActive(true);
    }
}
