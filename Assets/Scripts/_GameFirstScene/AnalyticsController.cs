﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using com.faithstudio.SDK;
using GameAnalyticsSDK;
//using LionStudios;

public class AnalyticsController : MonoBehaviour
{
    public static AnalyticsController analyticsController;

    //FacebookAnalyticsManager fbAnalytics;
    int levelcount = 1;

    GameManager gameManager;
    private void Awake()
    {
        levelcount = 1;
        analyticsController = this;
        gameManager = transform.GetComponent<GameManager>();
    }
    private void Start()
    {
        //fbAnalytics = FacebookAnalyticsManager.Instance;
        levelcount = gameManager.GetDataManager().GetGamePlayer.levelsCompleted;
        GameAnalytics.Initialize();
    }

    public static AnalyticsController GetController()
    {
        return analyticsController;
    }

    public void LevelStarted()
    {
        //LionStudios.Analytics.Events.LevelStarted(levelcount);
        //fbAnalytics.FBALevelStart(levelcount);
        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Start, "World01", "Level " + levelcount);
        SaveGame();
    }
    public void LevelCompleted()
    {
        //LionStudios.Analytics.Events.LevelComplete(levelcount);
        //fbAnalytics.FBALevelComplete(levelcount);
        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Complete, "World01", "Level " + levelcount);
        levelcount++;
        SaveGame();
    }
    public void LevelFailed()
    {
        //LionStudios.Analytics.Events.LevelFailed(levelcount);
        //fbAnalytics.FBALevelFailed(levelcount);
        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Fail, "World01", "Level " + levelcount);
    }
    public void SaveGame()
    {
        GamePlayer gp = new GamePlayer();
        gp.name = "";
        gp.id = 1;
        gp.levelsCompleted = levelcount;
        gp.totalCoins = 0;// GameController.GetController().GetTotalCoins();
        gp.lastPlayedLevel = gameManager.GetlevelCount();
        gp.handTutorialShown = true;

        gameManager.GetDataManager().SetGameplayerData(gp);
    }

}
