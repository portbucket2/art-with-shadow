﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class CameraController : MonoBehaviour
{

    [SerializeField] float transitionTime;
    [SerializeField] Vector3 showPosition;
    [SerializeField] Vector3 showRotation;


    [Header("debug: ")]
    [SerializeField] Transform mainCamera;
    [SerializeField] Vector3 startPosition;
    [SerializeField] Vector3 startRotation;

    [SerializeField] Transform currentTarget;
    [SerializeField] bool isLookingAt = false;

    GameController gameController;

    private void Awake()
    {
        
    }
    void Start()
    {
        //startPosition = transform.localPosition;
        //startRotation = transform.localEulerAngles;
        gameController = GameController.GetController();
        ShowOffSubject();
    }
    private void Update()
    {
        //if (isLookingAt)
        //{
        //    mainCamera.LookAt(currentTarget);
        //}
    }


    void ShowOffSubject()
    {
        Vector3 rot = new Vector3(0f, 90f, 0f);
        //transform.DOLocalRotate(rot, 0.1f).SetEase(Ease.Flash);
        transform.localEulerAngles = rot;

        currentTarget = gameController.GetSceneController().GetCurrentArtSubject();
        //isLookingAt = true;
        mainCamera.DOLocalMove(showPosition, transitionTime).SetEase(Ease.OutSine).OnComplete(PanCamera);
        mainCamera.DOLocalRotate(showRotation, transitionTime).SetEase(Ease.OutSine);
    }


    void PanCamera()
    {
        Vector3 rot = new Vector3(0f, 90f, 0f);
        transform.DOLocalRotate(rot, transitionTime * 3f).SetEase(Ease.InCubic).OnComplete(PanComplete);
        transform.DOLocalRotate(Vector3.zero, transitionTime * 3f).SetEase(Ease.InSine);
    }
    void PanComplete()
    {
        mainCamera.DOLocalMove(startPosition, transitionTime).SetEase(Ease.OutSine);
        mainCamera.DOLocalRotate(startRotation, transitionTime).SetEase(Ease.OutSine).OnComplete(AllComplete);
    }

    void AllComplete()
    {
        //mainCamera.localPosition = startPosition;
        //mainCamera.localEulerAngles = startRotation;

        transform.DOKill();
        mainCamera.DOKill();
        gameController.GetLightPivot().EnableInput();
    }

    public void SetSkyColor(Color _color)
    {
        mainCamera.GetComponent<Camera>().backgroundColor = _color;
    }
}
