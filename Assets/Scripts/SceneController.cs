﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneController : MonoBehaviour
{
    [SerializeField] GameDataSheet data;
    [Header("scene stuffs")]
    [SerializeField] Transform bgPlane;
    [SerializeField] Transform imagePlane;
    [Header("Debug: ")]
    [SerializeField] LightPivot lightPivot;
    [SerializeField] int currentLevel = 0;
    [SerializeField] Transform currentArtSubject;

    GameController gameController;

    LevelData levelData;

    readonly string COLOR = "_Color";
    readonly string SHADOWCOLOR = "_Shadow_Color";
    readonly string MAINTEXTURE = "_MainTexture";
    GameManager gameManager;
    AnalyticsController analytics;
    void Start()
    {
        gameManager = GameManager.GetManager();
        analytics = AnalyticsController.GetController();
        gameController = GameController.GetController();
        if (currentLevel < 0)
        {
            currentLevel = gameManager.GetlevelCount();
        }        
        levelData = data.levelDatas[currentLevel];
        //levelData = data.levelDatas[0];
        lightPivot = gameController.GetLightPivot();

        gameController.GetCameraController().SetSkyColor(levelData.sceneThemeColor);

        SetLevelDatas();
        analytics.LevelStarted();
    }

    void SetLevelDatas()
    {
        GameObject gg = Instantiate(levelData.artSubject);
        currentArtSubject = gg.transform;
        //currentArtSubject.transform.position = levelData.artSubjectPosition;
        //currentArtSubject.transform.eulerAngles = levelData.artSubjectPosition;
        //currentArtSubject.transform.localScale = levelData.artSubjectScale;
        Material bgMat = bgPlane.GetComponent<MeshRenderer>().material;
        bgMat.SetColor(COLOR, levelData.sceneThemeColor);
        bgMat.SetColor(SHADOWCOLOR, levelData.shadowColor);

        Material imageMat = imagePlane.GetComponent<MeshRenderer>().material;
        imageMat.SetColor(COLOR, levelData.sceneThemeColor);
        imageMat.SetColor(SHADOWCOLOR, levelData.shadowColor);
        imageMat.SetTexture(MAINTEXTURE, levelData.shadowArt);

        lightPivot.SetLightAngle(levelData);
    }
    public Transform GetCurrentArtSubject()
    {
        return currentArtSubject;
    }
}

