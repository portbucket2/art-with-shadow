﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public static GameController gameController;

    

    [SerializeField] LightPivot lightPivot;
    [SerializeField] SceneController sceneController;
    [SerializeField] UIController uiController;
    [SerializeField] ParticleSystem ConfettiParticle;
    [SerializeField] CameraController cameraController;

    private void Awake()
    {
        gameController = this;
    }
    public static GameController GetController()
    {
        return gameController;
    }

    public LightPivot GetLightPivot() { return lightPivot; }
    public SceneController GetSceneController() { return sceneController; }
    public UIController GetUIController() { return uiController; }
    public CameraController GetCameraController() { return cameraController; }

    WaitForSeconds WAITONE = new WaitForSeconds(1f);

    void Start()
    {
        
    }

    public void LevelComplete()
    {
        StartCoroutine(LevelCompleteRoutine());
    }
    IEnumerator LevelCompleteRoutine()
    {
        ConfettiParticle.Play();
        yield return WAITONE;
        uiController.LevelComplete();
    }

}
