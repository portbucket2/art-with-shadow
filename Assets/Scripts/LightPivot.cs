﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class LightPivot : MonoBehaviour
{
    [SerializeField] bool inputEnabled = false;
    [SerializeField] float Speed = 0.1f;
    [SerializeField] float targetvalue = 100f;
    [SerializeField] float snapDistance = 3f;
    [SerializeField] Transform DirectionaleLight;
    [SerializeField] Transform secondSpotLightHolder;
    [SerializeField] Transform secondSpotLight;
    Vector3 mouseStart;
    GameController gameController;
    float h = 0;
    void Start()
    {
        gameController = GameController.GetController();
    }

    // Update is called once per frame
    void Update()
    {
        if (!inputEnabled)
            return;

        if (Input.GetMouseButtonDown(0))
        {
            mouseStart = Input.mousePosition;
        }
        if (Input.GetMouseButton(0))
        {
            SetRotationOfLight(Vector3.Distance(mouseStart, Input.mousePosition));
        }
        if (Input.GetMouseButtonUp(0))
        {

        }
    }

    void SetRotationOfLight(float distance)
    {
        if (distance > 0.1f)
        {
            h = Speed * Input.GetAxis("Mouse X");
            transform.eulerAngles += new Vector3(0f, -h * Time.deltaTime * Speed, 0f);
        }
        TargetCheck();
    }

    void TargetCheck()
    {
        Vector3 cc = transform.eulerAngles;
        Vector3 tt = new Vector3(0f, targetvalue, 0f);
        if (Vector3.Distance(cc, tt) < snapDistance ) 
        {
            Debug.Log("level Complete");
            inputEnabled = false;
            Snap();
        }
    }
    void Snap()
    {
        transform.DOLocalRotate(new Vector3(0f,targetvalue,0f),0.35f).OnComplete(LevelComplete);
    }
    void LevelComplete()
    {
        gameController.LevelComplete();
    }
    public void SetLightAngle(LevelData _levelData)
    {
        LevelData data = _levelData;
        float steepAngle = data.startHeightAngle;
        float target = data.targetRotationAngle;
        float secondRotationAngle = data.secondLightRotationAngle;
        Vector2 range = data.startRotationAngle;
        
        targetvalue = target;
        DirectionaleLight.localEulerAngles = new Vector3(steepAngle, 0f, 0f);
        transform.DOLocalRotate(new Vector3(0f, Random.Range(range.x, range.y), 0f), 0.35f);
        if (secondRotationAngle > 0)
        {
            Debug.Log("second: " + secondRotationAngle);
            secondSpotLightHolder.gameObject.SetActive(true);
            secondSpotLightHolder.DOLocalRotate(new Vector3(0f, secondRotationAngle, 0f), 0f);
            float y = data.secondLightHeight;
            float z = data.secondLightDistance;
            secondSpotLight.localPosition = new Vector3(0f, y, z);
        }
        else
        {
            secondSpotLightHolder.gameObject.SetActive(false);
        }
    }
    public void EnableInput()
    {
        inputEnabled = true;
    }
}
